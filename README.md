# 1000-fragments

Dépôt public sur lequel sont accessibles les données issues de Geocyclab, un atelier nomade ayant sillonné les routes du monde entre 2012 et 2015. Ces données sont au cœur d'une œuvre hybride : **[1000](https://exsitu.xyz/prod/creation/meta-projet/back-up)**

Boite noire du projet **[Geocyclab](https://exsitu.xyz/prod/creation/meta-projet)**, journal de bord matérialisé ou cabinet de curiosité nomade à l’ère du technocène et du numérique, **1000** est une installation multimédia dont l'esthétique s'inspire volontairement de celle des data-centers. Elle abrite, explore et ré-agence une base de données résultante d'un travail systématique et rigoureux de longue haleine, la collecte de 1000 objets et de plusieurs données qui leurs sont associées.

Ce dépot met à disposition de toute personne curieuse et créative les données relatives à notre collection d'objets. 
Diffusée sous licence Creative Commons - CC BY-NC-SA
